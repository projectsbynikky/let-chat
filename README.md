# Installation

### Running Locally

Make sure you have Node.js and npm install.

1. Install Dependencies
      <pre>npm install</pre>

2. Start the Application
     <pre>npm start</pre>

     Application runs from localhost:3000.

    No database communication 

## Sockets
    
   Having an active connection opened between the client and the server so client can send and receive data. This allows             real-time communication using TCP sockets. This is made possible by Socket.io.

   The client starts by connecting to the server through a socket(maybe also assigned to a specific namespace). Once connections is successful, client and server can emit and listen to events. 

   